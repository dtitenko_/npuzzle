import heapq
from math import sqrt


class Node:
    def __init__(self, grid):
        self.grid = grid
        self.value = 0
        self.depth = 0
        self.order = None
        self.parent = None
        self.adj_matrix = []

    def __lt__(self, other):
        return self.value < other.value


def getvalue(tab, key):
    sqrt_size = int(sqrt(len(tab)))
    length = int(len(tab))
    values = [1, -1, sqrt_size, -sqrt_size]
    valid = []
    for x in values:
        if 0 <= key + x < length:
            if x == 1 and key in range(sqrt_size - 1, length, sqrt_size):
                continue
            if x == -1 and key in range(0, length, sqrt_size):
                continue
            valid.append(x)
    return valid


def children(current, goal, heuristic):
    expands = {}
    length = len(current[1].grid)
    for key in range(length):
        expands[key] = getvalue(current[1].grid, key)
    pos = current[1].grid.index(0)
    moves = expands[pos]
    exp_stat = []
    children_ = []
    for mv in moves:
        n_state = current[1].grid[:]
        (n_state[pos + mv], n_state[pos]) = (n_state[pos], n_state[pos + mv])
        exp_stat.append(n_state)
        child = Node(n_state)
        child.value = heuristic(child.grid, goal)
        child.depth = current[1].depth + 1
        children_.append(Node(n_state))
    return children_


def search_grid_in_set(grid, list_set):
    for elem in list_set:
        if elem.grid == grid:
            return elem.depth
    return 0


def search_grid_in_tuple(grid, list_set):
    for elem in list_set:
        if elem[1].grid == grid:
            return elem[1].depth
    return 0


def search_grid_in_set_and_remove(grid, list_set):
    for elem in list_set:
        if elem[1].grid == grid:
            list_set.remove(elem)
            return elem
    new_node = Node(grid)
    return new_node


def a_star(start, goal, heuristic):
    open_set = []
    max_nb_states_in_memory_at_the_same_time = 0
    max_nb_states_in_open_at_the_same_time = 0
    closed_set = set()
    current = Node(start)
    current.value = heuristic(start, goal)
    current.depth = 1
    heapq.heappush(open_set, (current.value, current))
    while open_set:
        current = heapq.heappop(open_set)
        if current[1].grid == goal:
            path = []
            current = current[1]
            while current.parent:
                path.append(current)
                current = current.parent
                current = current[1]
            path.append(current)
            return path[::-1], max_nb_states_in_memory_at_the_same_time, max_nb_states_in_open_at_the_same_time
        closed_set.add(current)
        for node in children(current, goal, heuristic):
            if search_grid_in_tuple(node.grid, closed_set) > 0:
                continue
            node_depth = search_grid_in_tuple(node.grid, open_set)
            if node_depth > 0:
                new_depth = current[1].depth + 1
                if node_depth > new_depth:
                    node_listed = search_grid_in_set_and_remove(node.grid, open_set)
                    node_listed[1].depth = new_depth
                    node_listed[1].parent = current
                    heapq.heappush(open_set, (node_listed[1].value, node_listed[1]))
            else:
                node.depth = current[1].depth + 1
                node.value = heuristic(node.grid, goal)
                node.parent = current
                heapq.heappush(open_set, (node.value, node))
        open_set_len = len(open_set)
        open_set_tot = len(open_set) + len(closed_set)
        if open_set_len > max_nb_states_in_open_at_the_same_time:
            max_nb_states_in_open_at_the_same_time = open_set_len
        if open_set_tot > max_nb_states_in_memory_at_the_same_time:
            max_nb_states_in_memory_at_the_same_time = open_set_tot
    raise ValueError('No Path Found')
