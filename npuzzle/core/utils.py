from math import sqrt


class FGColors:
    MAGENTA = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    END = '\033[0m'


def print_tab(tab):
    width = int(sqrt(len(tab)))
    length = int(len(tab))
    for (index, value) in enumerate(tab):
        if value == 0:
            print(FGColors.RED + '%4s' % value, end=" ")
        elif value <= 9:
            print(FGColors.MAGENTA + '%4s' % value, end=" ")
        elif value <= 19:
            print(FGColors.BLUE + '%4s' % value, end=" ")
        elif value <= 29:
            print(FGColors.GREEN + '%4s' % value, end=" ")
        else:
            print(FGColors.YELLOW + '%4s' % value, end=" ")
        if index in [x for x in range(width - 1, length, width)]:
            print()
    print(FGColors.END)


def write_tab(tab, f):
    print(tab, file=f)


def spiral(n):
    from npuzzle.core.generator import make_goal
    print(make_goal(n))
    dx, dy = 1, 0
    x, y = 0, 0
    arr = [[None] * n] * n
    for i in range(n ** 2):
        if i + 1 == n ** 2:
            arr[x][y] = 0
        else:
            arr[x][y] = i + 1
        nx, ny = x + dx, y + dy
        if 0 <= nx < n and 0 <= ny < n and arr[nx][ny] is None:
            x, y = nx, ny
        else:
            dx, dy = -dy, dx
            x, y = x + dx, y + dy
    return arr


def create_spiral(arr):
    n = range(len(arr))
    goal = []
    for y in n:
        for x in n:
            goal.append(arr[x][y])
    return goal


