import sys


class Parsing:

    def __init__(self, file):
        self.puzzle = None
        self.puzzle_size = None
        self.parse(file)

    def parse(self, file):
        line_i = 0
        self.puzzle = []
        for line in file.readlines():
            splitted_line = line.split()

            hashtag_in_middle = False
            word_hashtag_i = None
            index_comment = None
            try:
                for word in splitted_line:
                    if '#' in word:
                        if word.index('#') == 0:
                            index_comment = splitted_line.index(word)
                        else:
                            index_comment = splitted_line.index(word) + 1
                            word_hashtag_i = word.index('#')
                            hashtag_in_middle = True
                        if index_comment == 0:
                            splitted_line = None
                        pass
            except Exception as ex:
                index_comment = None

            if splitted_line == None or all(splitLine.isspace() for splitLine in splitted_line):
                continue

            splitted_line = splitted_line[:index_comment]
            if hashtag_in_middle:
                splitted_line[len(splitted_line) - 1] = splitted_line[len(splitted_line) - 1][:word_hashtag_i]
            if line_i == 0:
                if len(splitted_line) > 1:
                    sys.exit('Invalid file format')
                try:
                    self.puzzle_size = int(splitted_line[0])
                except Exception as ex:
                    sys.exit('Invalid file format')
            else:
                if len(splitted_line) != self.puzzle_size:
                    sys.exit('Invalid file format')
                word_i = 0
                for word in splitted_line:
                    try:
                        self.puzzle.append(int(splitted_line[word_i]))
                        word_i += 1
                    except Exception as ex:
                        sys.exit('Invalid file format')

            line_i += 1

        if len(self.puzzle) != self.puzzle_size * self.puzzle_size:
            sys.exit('Invalid puzzle format')

        for nb in range(0, self.puzzle_size * self.puzzle_size - 1):
            if self.puzzle.count(nb) != 1:
                sys.exit('Invalid puzzle format')
