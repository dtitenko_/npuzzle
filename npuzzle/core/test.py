from npuzzle.core.heuristics import manhattan


def is_resolvable(tab, goal):
    nb_inv = 0
    test_done = set()
    for index_goal in (range(0, len(goal) - 1)):
        for index_puzzle in (range(0, tab.index(goal[index_goal]))):
            if tab[index_puzzle] not in test_done:
                nb_inv += 1
        test_done.add(goal[index_goal])
    dist = manhattan(tab, goal)
    if dist % 2 == 0 and nb_inv % 2 == 0 or dist % 2 != 0 and nb_inv % 2 != 0:
        return True
    return False
