from math import sqrt


def manhattan(tab, goal):
    dist = 0
    n_size = int(sqrt(len(tab)))
    for node in tab:
        if node != 0 and goal.index(node) != tab.index(node):
            x_goal = goal.index(node) // n_size
            y_goal = goal.index(node) % n_size
            x_tab = tab.index(node) // n_size
            y_tab = tab.index(node) % n_size
            dist += abs(x_goal - x_tab) + abs(y_goal - y_tab)
    return dist


def euclidean(tab, goal):
    dist = 0
    n_size = int(sqrt(len(tab)))
    for node in tab:
        if node != 0 and goal.index(node) != tab.index(node):
            x_goal = goal.index(node) // n_size
            y_goal = goal.index(node) % n_size
            x_tab = tab.index(node) // n_size
            y_tab = tab.index(node) % n_size
            dist += sqrt(pow((x_goal - x_tab), 2) + pow((y_goal - y_tab), 2))
    return dist


def linear_conflict(puzzle, goal):
    puzzle_row_size = int(sqrt(len(puzzle)))
    nb_linear_conflict = 0
    for square in puzzle:
        if square != 0 and puzzle.index(square) != goal.index(square):
            goal_square_i = goal.index(square)
            puzzle_square_i = puzzle.index(square)
            if puzzle_square_i // puzzle_row_size == goal_square_i // puzzle_row_size:
                puzzle_square_row = puzzle_square_i // puzzle_row_size
                for puzzle_next_square_i in range(puzzle_row_size * puzzle_square_row, puzzle_square_i + (puzzle_row_size * (puzzle_square_row + 1) - puzzle_square_i) - 1):
                    if puzzle_next_square_i != 0 \
                            and puzzle_next_square_i != puzzle_square_i \
                            and puzzle_next_square_i // puzzle_row_size == goal.index(puzzle[puzzle_next_square_i]) // puzzle_row_size:
                        goal_next_square_i = goal.index(puzzle[puzzle_next_square_i])
                        if (puzzle_next_square_i > puzzle_square_i and goal_next_square_i < goal_square_i) \
                                or (puzzle_next_square_i < puzzle_square_i and goal_next_square_i > goal_square_i):
                            nb_linear_conflict += 1
            if puzzle_square_i % puzzle_row_size == goal_square_i % puzzle_row_size:
                for puzzle_next_square_i in range(puzzle_square_i, len(puzzle) - 1, puzzle_row_size):
                    if puzzle_next_square_i != 0 and puzzle_next_square_i != puzzle_square_i and \
                            puzzle_next_square_i % puzzle_row_size == goal.index(puzzle[puzzle_next_square_i]) % puzzle_row_size:
                        goal_next_square_i = goal.index(puzzle[puzzle_next_square_i])
                        if (puzzle_next_square_i > puzzle_square_i and goal_next_square_i < goal_square_i) \
                                or (puzzle_next_square_i < puzzle_square_i and goal_next_square_i > goal_square_i):
                            nb_linear_conflict += 1
    return manhattan(puzzle, goal) + nb_linear_conflict


def misplaced_tiles(puzzle, goal):
    nb_misplaced_tiles = 0
    for square in puzzle:
        if square != 0 and puzzle.index(square) != goal.index(square):
            nb_misplaced_tiles += 1
    return nb_misplaced_tiles


def heuristic(heuristics_name):
    if heuristics_name == 'mh':
        return manhattan
    if heuristics_name == 'eu':
        return euclidean
    if heuristics_name == 'lc':
        return linear_conflict
    if heuristics_name == 'mt':
        return misplaced_tiles
