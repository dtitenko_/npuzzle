#!/usr/bin/env python
import argparse
import os
import sys
import time

from npuzzle.core.heuristics import heuristic
from npuzzle.core.parser import Parsing
from npuzzle.core.solver import a_star
from npuzzle.core.test import is_resolvable
from npuzzle.core.utils import *
from npuzzle.core.generator import *


def parse_argument(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--animate', action='store_true', default=False)
    parser.add_argument('-H', '--heuristic', choices=['mh', 'eu', 'lc', 'mt'], default='mh')

    group1 = parser.add_mutually_exclusive_group()
    group1.add_argument('-l', '--length', default='3', type=int, help='Size of your puzzle between 3 and 70')
    group1.add_argument('-f', '--file', type=argparse.FileType(), default=None, help='Path to file with n-puzzle')

    group2 = parser.add_mutually_exclusive_group()
    group2.add_argument('-s', '--solvable', action='store_true', default=True, help='Forces generation of a solvable puzzle.')
    group2.add_argument('-u', '--unsolvable', action='store_true', default=False, help='Forces generation of an unsolvable puzzle.')

    return parser.parse_args(args)


def main():
    arg = parse_argument(sys.argv[1:])

    if arg.file:
        parsing = Parsing(arg.file)
        start = parsing.puzzle
        goal = make_goal(int(sqrt(len(start))))

    else:
        if 2 < arg.length < 71:
            goal = make_goal(arg.length)
            solvable = None
            if arg.solvable:
                solvable = True
            elif arg.unsolvable:
                solvable = False
            start = make_puzzle(arg.length, solvable, 1000)
        else:
            print('Wrong size')
            sys.exit(1)

    print('The Goal State is:')
    print_tab(goal)
    print('The Starting State is:')
    print_tab(start)
    if not is_resolvable(start, goal):
        print("UNSOLVABLE")
        sys.exit(1)
    start_time = time.time()
    path, max_in_memory, max_in_open = a_star(start, goal, heuristic(arg.heuristic))
    end_time = time.time()

    if arg.animate:
        for elem in path:
            time.sleep(1)
            os.system('clear')
            print_tab(elem.grid)
    else:
        for elem in path:
            print_tab(elem.grid)

    print(f"Complexity in size: {max_in_memory}")
    print(f"Complexity in time: {max_in_open}")
    print(f"Number of step: {len(path)}")
    print(f"Completed in: {end_time - start_time:.2f}s")


if __name__ == '__main__':
    main()
