#!/usr/bin/env python

from setuptools import setup, find_packages

release = "1.0"

install_requires = []

dev_requires = []

tst_requires = []

setup(
    name='n-puzzle',
    version=release,
    author='',
    author_email='dtitenko@student.unit.ua',
    description='N-Puzzle Project',
    long_description=open('README.md').read(),
    packages=find_packages(),
    install_requires=install_requires + tst_requires,
    tests_require=tst_requires,
    extras_require={
        'dev': dev_requires,
    },
    entry_points={
        'console_scripts': ('n-puzzle = npuzzle:main',),
    },
    include_package_data=True,
    classifiers=[
        'Intended Audience :: Developers',
        'Intended Audience :: Students',
        'Operating System :: OS Independent',
    ],
)
